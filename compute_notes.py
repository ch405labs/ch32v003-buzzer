#!/usr/bin/env python3

"""
Just generate #defines to be used for notes in the melody library.
"""

if __name__ == '__main__':
    c0 = 440*2**(-4)*2**(-9/12)
    print(f'#define REF_C0 ({c0})')
    print()

    for octave in range(9):
        for semitone,note in enumerate('C CS D DS E F FS G GS A AS B'.split(' ')):
            name = note+str(octave)
            freq = c0*2**(octave+semitone/12)
            # It's not necessary to push precision to below 1Hz, the final sound is off by ~8%...
            #formula = f'(TICKS_PER_SEC/{int(round(freq))})'
            #formula += ' '*(22-len(formula))
            print(f'#define NOTE_{name:<5} {"("+str(octave*12+semitone)+")":<6} /**< {note}_{octave}{" sharp" if len(note)>1 else ""} {freq:.02f} Hz */')
        print()

    #for octave in range(9):
    #    for note in 'C CS D DS E F FS G GS A AS B'.split(' '):
    #        name = note+str(octave)
    #        print(f'    {{NOTE_{name}, 1, 8}},')

    fx = [f'{round(c0*2**(8+i/12))}' for i in range(12)]
    print('const uint16_t FREQ_x8[] = {', ', '.join(fx), '};')
