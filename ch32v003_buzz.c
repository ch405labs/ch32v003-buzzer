#include "ch32v003_buzz.h"

// Buzzer

void buzzer_init() {
    GPIO_port_enable(PORT_BUZZ);
    GPIO_pinMode(GPIOv_from_PORT_PIN(PORT_BUZZ, PIN_BUZZ), GPIO_pinMode_O_pushPull, GPIO_Speed_10MHz);
    GPIO_digitalWrite(GPIOv_from_PORT_PIN(PORT_BUZZ, PIN_BUZZ), high);
}

/**
 * @brief Plays a note for a given duration in ms
 */
void buzzer_sound(uint16_t freq, uint8_t duration_numerator, uint8_t duration_denominator, uint8_t bpm) {
  int32_t duration_us = ((uint32_t)(duration_numerator * 60000000)/(uint32_t)(duration_denominator * bpm));
  if(freq == 0) {
    // Pause (frequency 0 = pause)
    Delay_Us(duration_us);
  } else {
    // Play note
    int32_t sq_wave_wait_us = 1000000 / (2*freq);
    while(duration_us > 0) {
      #if BUZZ_SOUND == 1
      if(freq) {
          GPIO_digitalWrite(GPIOv_from_PORT_PIN(PORT_BUZZ, PIN_BUZZ), low);
      }
      #endif
      Delay_Us(sq_wave_wait_us);
      GPIO_digitalWrite(GPIOv_from_PORT_PIN(PORT_BUZZ, PIN_BUZZ), high);
      Delay_Us(sq_wave_wait_us);
      duration_us -= (sq_wave_wait_us * 2) + 5;                                     // TODO - 5 is const estimated us for loop execution 
    }
  }
}