/**
 * @file    ssd1306_oled.h
 * @author  Florian Schütz (fschuetz@ieee.org) (based on E. Brombaugh original)
 * @brief   Header for ssd1306 based oleds.
 * @version 1.0
 * @date    09.12.2023
 * 
 * @copyright Copyright (c) 2023 Florian Schütz, released under MIT license
 * 
 * Based on original from 2022 by Stefan Wagner: https://github.com/wagiminator
 * 
 */
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "ch32v003fun.h"
#include "ch32v003_GPIO_branchless.h"

#ifndef PORT_BUZZ
#define PORT_BUZZ               GPIO_port_A
#endif //PORT_BUZZ

#ifndef PIN_BUZZ
#define PIN_BUZZ                    1                                           /**< pin connected to buzzer */
#endif //PIN_BUZZ

#ifndef BUZZ_SOUND
#define BUZZ_SOUND                  1                                           /**< 0: no sound, 1: with sound */
#endif //BUZZ_SOUND

#define BUZZ_C0                     16                                          /**< C_0 16.05 Hz */
#define BUZZ_CS0                    17                                          /**< C_0 sharp 17.01 Hz */
#define BUZZ_DM0                    BUZZ_CS0                                    /**< D_0 minor 17.01 Hz */
#define BUZZ_D0                     18                                          /**< D_0 18.02 Hz */
#define BUZZ_DS0                    19                                          /**< D_0 sharp 19.09 Hz */
#define BUZZ_EM0                    BUZZ_DS0                                    /**< E_0 minor 19.09 Hz */
#define BUZZ_E0                     20                                          /**< E_0 20.23 Hz */
#define BUZZ_F0                     21                                          /**< F_0 21.43 Hz */
#define BUZZ_FS0                    23                                          /**< F_0 sharp 22.70 Hz */
#define BUZZ_GM0                    BUZZ_FS0                                    /**< G_0 minor 22.70 Hz */
#define BUZZ_G0                     24                                          /**< G_0 24.05 Hz */
#define BUZZ_GS0                    25                                          /**< G_0 sharp 25.48 Hz */
#define BUZZ_AM0                    BUZZ_GS0                                    /**< A_0 minor 25.48 Hz */
#define BUZZ_A0                     27                                          /**< A_0 27.00 Hz */
#define BUZZ_AS0                    29                                          /**< A_0 sharp 28.61 Hz */
#define BUZZ_BM0                    BUZZ_AS0                                    /**< B_0 minor 28.61 Hz */
#define BUZZ_B0                     30                                          /**< B_0 30.31 Hz */

#define BUZZ_C1                     32                                          /**< C_1 32.11 Hz */
#define BUZZ_CS1                    34                                          /**< C_1 sharp 34.02 Hz */
#define BUZZ_DM1                    BUZZ_CS1                                    /**< D_1 minor 34.02 Hz */
#define BUZZ_D1                     36                                          /**< D_1 36.04 Hz */
#define BUZZ_DS1                    38                                          /**< D_1 sharp 38.18 Hz */
#define BUZZ_EM1                    BUZZ_DS1                                    /**< E_1 minor 38.18 Hz */
#define BUZZ_E1                     40                                          /**< E_1 40.45 Hz */
#define BUZZ_F1                     43                                          /**< F_1 42.86 Hz */
#define BUZZ_FS1                    45                                          /**< F_1 sharp 45.41 Hz */
#define BUZZ_GM1                    BUZZ_FS1                                    /**< G_1 minor 45.41 Hz */
#define BUZZ_G1                     48                                          /**< G_1 48.11 Hz */
#define BUZZ_GS1                    51                                          /**< G_1 sharp 50.97 Hz */
#define BUZZ_AM1                    BUZZ_GS1                                    /**< A_1 minor 50.97 Hz */
#define BUZZ_A1                     54                                          /**< A_1 54.00 Hz */
#define BUZZ_AS1                    57                                          /**< A_1 sharp 57.21 Hz */
#define BUZZ_BM1                    BUZZ_AS1                                    /**< B_1 minor 57.21 Hz */
#define BUZZ_B1                     61                                          /**< B_1 60.61 Hz */

#define BUZZ_C2                     64                                          /**< C_2 64.22 Hz */
#define BUZZ_CS2                    68                                          /**< C_2 sharp 68.04 Hz */
#define BUZZ_DM2                    BUZZ_CS2                                    /**< D_2 minor 68.04 Hz */
#define BUZZ_D2                     72                                          /**< D_2 72.08 Hz */
#define BUZZ_DS2                    76                                          /**< D_2 sharp 76.37 Hz */
#define BUZZ_EM2                    BUZZ_DS2                                    /**< E_2 minor 76.37 Hz */
#define BUZZ_E2                     81                                          /**< E_2 80.91 Hz */
#define BUZZ_F2                     86                                          /**< F_2 85.72 Hz */
#define BUZZ_FS2                    91                                          /**< F_2 sharp 90.82 Hz */
#define BUZZ_GM2                    BUZZ_FS2                                    /**< G_2 minor 90.82 Hz */
#define BUZZ_G2                     96                                          /**< G_2 96.22 Hz */
#define BUZZ_GS2                    102                                         /**< G_2 sharp 101.94 Hz */
#define BUZZ_AM2                    BUZZ_GS2                                    /**< A_2 minor 101.94 Hz */
#define BUZZ_A2                     108                                         /**< A_2 108.00 Hz */
#define BUZZ_AS2                    114                                         /**< A_2 sharp 114.42 Hz */
#define BUZZ_BM2                    BUZZ_AS2                                    /**< B_2 minor 114.42 Hz */
#define BUZZ_B2                     121                                         /**< B_2 121.23 Hz */

#define BUZZ_C3                     128                                         /**< C_3 128.43 Hz */
#define BUZZ_CS3                    136                                         /**< C_3 sharp 136.07 Hz */
#define BUZZ_DM3                    BUZZ_CS3                                    /**< D_3 minor 136.07 Hz */
#define BUZZ_D3                     144                                         /**< D_3 144.16 Hz */
#define BUZZ_DS3                    153                                         /**< D_3 sharp 152.74 Hz */
#define BUZZ_EM3                    BUZZ_DS3                                    /**< E_3 minor 152.74 Hz */
#define BUZZ_E3                     162                                         /**< E_3 161.82 Hz */
#define BUZZ_F3                     171                                         /**< F_3 171.44 Hz */
#define BUZZ_FS3                    182                                         /**< F_3 sharp 181.63 Hz */
#define BUZZ_GM3                    BUZZ_FS3                                    /**< G_3 minor 181.63 Hz */
#define BUZZ_G3                     192                                         /**< G_3 192.43 Hz */
#define BUZZ_GS3                    204                                         /**< G_3 sharp 203.88 Hz */
#define BUZZ_AM3                    BUZZ_GS3                                    /**< A_3 minor 203.88 Hz */
#define BUZZ_A3                     216                                         /**< A_3 216.00 Hz */
#define BUZZ_AS3                    229                                         /**< A_3 sharp 228.84 Hz */
#define BUZZ_BM3                    BUZZ_AS3                                    /**< B_3 minor 228.84 Hz */
#define BUZZ_B3                     242                                         /**< B_3 242.45 Hz */

#define BUZZ_C4                     257                                         /**< C_4 256.87 Hz */
#define BUZZ_CS4                    272                                         /**< C_4 sharp 256.87 Hz */
#define BUZZ_DM4                    BUZZ_CS4                                    /**< D_4 minor 146.07 Hz */
#define BUZZ_D4                     288                                         /**< D_4 288.33 Hz */
#define BUZZ_DS4                    305                                         /**< D_4 sharp 305.47 Hz */
#define BUZZ_EM4                    BUZZ_DS4                                    /**< E_4 minor 152.74 Hz */
#define BUZZ_E4                     324                                         /**< E_4 323.63 Hz */
#define BUZZ_F4                     343                                         /**< F_4 342.88 Hz */
#define BUZZ_FS4                    363                                         /**< F_4 sharp 363.27 Hz */
#define BUZZ_GM4                    BUZZ_FS4                                    /**< G_4 minor 181.64 Hz */
#define BUZZ_G4                     385                                         /**< G_4 384.87 Hz */
#define BUZZ_GS4                    408                                         /**< G_4 sharp 407.75 Hz */
#define BUZZ_AM4                    BUZZ_GS4                                    /**< A_4 minor 204.88 Hz */
#define BUZZ_A4                     432                                         /**< A_4 432.00 Hz */
#define BUZZ_AS4                    458                                         /**< A_4 sharp 457.69 Hz */
#define BUZZ_BM4                    BUZZ_AS4                                    /**< B_4 minor 228.84 Hz */
#define BUZZ_B4                     485                                         /**< B_4 484.90 Hz */

#define BUZZ_C5                     514                                         /**< C_5 513.74 Hz */
#define BUZZ_CS5                    544                                         /**< C_5 sharp 544.29 Hz */
#define BUZZ_DM5                    BUZZ_CS5                                    /**< D_5 minor 146.07 Hz */
#define BUZZ_D5                     577                                         /**< D_5 576.65 Hz */
#define BUZZ_DS5                    611                                         /**< D_5 sharp 610.94 Hz */
#define BUZZ_EM5                    BUZZ_DS5                                    /**< E_5 minor 152.74 Hz */
#define BUZZ_E5                     647                                         /**< E_5 647.27 Hz */
#define BUZZ_F5                     686                                         /**< F_5 685.76 Hz */
#define BUZZ_FS5                    727                                         /**< F_5 sharp 726.53 Hz */
#define BUZZ_GM5                    BUZZ_FS5                                    /**< G_5 minor 181.64 Hz */
#define BUZZ_G5                     770                                         /**< G_5 769.74 Hz */
#define BUZZ_GS5                    816                                         /**< G_5 sharp 815.51 Hz */
#define BUZZ_AM5                    BUZZ_GS5                                    /**< A_5 minor 204.88 Hz */
#define BUZZ_A5                     864                                         /**< A_5 864.00 Hz */
#define BUZZ_AS5                    915                                         /**< A_5 sharp 915.38 Hz */
#define BUZZ_BM5                    BUZZ_AS5                                    /**< B_5 minor 228.84 Hz */
#define BUZZ_B5                     970                                         /**< B_5 969.81 Hz */

#define BUZZ_C6                     1027                                        /**< C_6 1027.47 Hz */
#define BUZZ_CS6                    1089                                        /**< C_6 sharp 1088.57 Hz */
#define BUZZ_DM6                    BUZZ_CS6                                    /**< D_6 minor 146.07 Hz */
#define BUZZ_D6                     1153                                        /**< D_6 1153.30 Hz */
#define BUZZ_DS6                    1222                                        /**< D_6 sharp 1221.88 Hz */
#define BUZZ_EM6                    BUZZ_DS6                                    /**< E_6 minor 152.74 Hz */
#define BUZZ_E6                     1295                                        /**< E_6 1294.54 Hz */
#define BUZZ_F6                     1372                                        /**< F_6 1371.51 Hz */
#define BUZZ_FS6                    1453                                        /**< F_6 sharp 1453.07 Hz */
#define BUZZ_GM6                    BUZZ_FS6                                    /**< G_6 minor 181.64 Hz */
#define BUZZ_G6                     1539                                        /**< G_6 1539.47 Hz */
#define BUZZ_GS6                    1631                                        /**< G_6 sharp 1631.01 Hz */
#define BUZZ_AM6                    BUZZ_GS6                                    /**< A_6 minor 1631.01 Hz */
#define BUZZ_A6                     1728                                        /**< A_6 1728.00 Hz */
#define BUZZ_AS6                    1831                                        /**< A_6 sharp 1830.75 Hz */
#define BUZZ_BM6                    BUZZ_AS6                                    /**< B_6 minor 1830.75 Hz */
#define BUZZ_B6                     1940                                        /**< B_6 1939.61 Hz */

#define BUZZ_C7                     2056                                        /**< C_7 2054.95 Hz */
#define BUZZ_CS7                    2177                                        /**< C_7 sharp 2177.14 Hz */
#define BUZZ_DM7                    BUZZ_CS7                                    /**< D_7 minor 2177.14 Hz */
#define BUZZ_D7                     2307                                        /**< D_7 2306.60 Hz */
#define BUZZ_DS7                    2444                                        /**< D_7 sharp 2443.76 Hz */
#define BUZZ_EM7                    BUZZ_DS7                                    /**< E_7 minor 2443.76 Hz */
#define BUZZ_E7                     2589                                        /**< E_7 2589.07 Hz */
#define BUZZ_F7                     2743                                        /**< F_7 2743.03 Hz */
#define BUZZ_FS7                    2906                                        /**< F_7 sharp 2906.14 Hz */
#define BUZZ_GM7                    BUZZ_FS7                                    /**< G_7 minor 2906.14 Hz */
#define BUZZ_G7                     3079                                        /**< G_7 3078.95 Hz */
#define BUZZ_GS7                    3262                                        /**< G_7 sharp 3262.03 Hz */
#define BUZZ_AM7                    BUZZ_GS7                                    /**< A_7 minor 3262.03 Hz */
#define BUZZ_A7                     3456                                        /**< A_7 3456.00 Hz */
#define BUZZ_AS7                    3662                                        /**< A_7 sharp 3661.50 Hz */
#define BUZZ_BM7                    BUZZ_AS7                                    /**< B_7 minor 3661.50 Hz */
#define BUZZ_B7                     3879                                        /**< B_7 3879.23 Hz */

#define BUZZ_C8                     4110                                        /**< C_8 4109.90 Hz */
#define BUZZ_CS8                    4354                                        /**< C_8 sharp 4354.29 Hz */
#define BUZZ_DM8                    BUZZ_CS8                                    /**< D_8 minor 4354.29 Hz */
#define BUZZ_D8                     4613                                        /**< D_8 4613.21 Hz */
#define BUZZ_DS8                    4888                                        /**< D_8 sharp 4887.52 Hz */
#define BUZZ_EM8                    BUZZ_DS8                                    /**< E_8 minor 4887.52 Hz */
#define BUZZ_E8                     5178                                        /**< E_8 5178.15 Hz */
#define BUZZ_F8                     5486                                        /**< F_8 5486.06 Hz */
#define BUZZ_FS8                    5812                                        /**< F_8 sharp 5812.28 Hz */
#define BUZZ_GM8                    BUZZ_FS8                                    /**< G_8 minor 5812.28 Hz */
#define BUZZ_G8                     6158                                        /**< G_8 6157.89 Hz */
#define BUZZ_GS8                    6524                                        /**< G_8 sharp 6524.06 Hz */
#define BUZZ_AM8                    BUZZ_GS8                                    /**< A_8 minor 6524.06 Hz */
#define BUZZ_A8                     6912                                        /**< A_8 6912.00 Hz */
#define BUZZ_AS8                    7323                                        /**< A_8 sharp 7323.01 Hz */
#define BUZZ_BM8                    BUZZ_AS8                                    /**< B_8 minor 7323.01 Hz */
#define BUZZ_B8                     7758                                        /**< B_8 7758.46 Hz */

typedef struct BUZZ_note {
    uint16_t frequency;
    uint8_t duration_numerator;
    uint8_t duration_denominator;
} BUZZ_note;


void buzzer_init();
void buzzer_sound(uint16_t frequency, uint8_t duration_numerator, uint8_t duration_denominator, uint8_t bpm);
#define buzzer_play_note(n,bpm)   buzzer_sound(n.frequency, n.duration_numerator, n.duration_denominator, bpm)


#ifdef __cplusplus
};
#endif
