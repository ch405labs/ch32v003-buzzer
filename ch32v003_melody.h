/**
 * @author  Miaou (based on Florian Schütz based on E. Brombaugh original)
 * @brief   Melody library for BCD_0o27
 *
 * @copyright Copyright (c) 2024 Miaou, released under MIT license
 * 
 * Based on buzzer library from 2023 Florian Schütz, released under MIT license
 * Based on original from 2022 by Stefan Wagner: https://github.com/wagiminator
 * 
 * The basis of this work is that SysTick interrupts are used to drive the GPIO
 * pin of the buzzer with the right timing. The period of each note is computed
 * in CPU ticks (defines NOTE_*).
 * The melody library helps computing the number of times the IRQ is called and
 * plans the next note to play (buffered) so that transition between notes is
 * smooth.
 */
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "ch32v003fun.h"
#include "ch32v003_GPIO_branchless.h"

#ifndef PORT_BUZZ
#define PORT_BUZZ               GPIO_port_A
#endif //PORT_BUZZ

#ifndef PIN_BUZZ
#define PIN_BUZZ                    1                                           /**< pin connected to buzzer */
#endif //PIN_BUZZ

#define DIVIDER_DURATION 8 /** Durations are divided by LOWEST_DENOM */

/** Setups the melofy module, must be called first */
void melody_init();

/**
 * @brief The buzzer interrupt sets the GPIO level according to the current melody.
 * 
 * Must be called from the SysTick_Handler()
 */
extern inline void melody_irq(void);

/**
 * The notes which are used to program a melody
 * 
 * Note: Packing and / or bit fields are less efficient space wise than just
 *       letting the compiler do its optimisation voodoo.
 */
typedef struct {
    uint8_t pitch; /**< The pitch, in number of semitones (e.g. A4 is 9+4*12) */
    uint8_t durationEigths; /**< Duration, in 1/LOWEST beats */
} Note;

/**
 * @brief Program a melody to be played, call melody_step() regularly to play it
 * @param notes A NULL terminated array of Note
 * @param bpm Tempo as beats per minutes (durations of notes are given in fractions of a beat)
 */
void melody_start(Note *notes, uint8_t bpm);

/** Stops the current melody from playing immediately (GPIO set high on the next half period) */
void melody_stop(void);

/** Mask for melody_step(): the next note has been queued */
#define MELODY_NEXT (1<<0)
/** Mask for melody_step(): the last note is playing, you can melody_start() a new one */
#define MELODY_END (1<<1)
/** Mask for melody_step(): no more sound is played, the melody is stalled */
#define MELODY_FINISHED (1<<2)
/** Mask for melody_step(): some pitch were too high, and the sound engine failed to keep up */
#define MELODY_FAILED (1<<3)

/**
 * @brief Program the next note of the melody if needed.
 * @return 0 when no programmed note,
 * @return MELODY_NEXT when a new note was programmed,
 * @return MELODY_NEXT|MELODY_END when the last note was programmed,
 * @return MELODY_END when the end of the array is reached,
 * @return MELODY_FINISHED|MELODY_END when the melody is finished playing,
 * @return MELODY_FINISHED|MELODY_NEXT means that you were too slow to call step, and the buzzer was paused,
 * @return MELODY_FINISHED|MELODY_FAILED means that some pitch was too high and we stopped playback.
 */
uint8_t melody_step(void);


/* ------ DEFINITIONS OF THE NOTES ------ */
#define TICKS_PER_SEC ((DELAY_MS_TIME)*1000)

#define SILENCE    (0)

//#define NOTE_C0    (0)    /**< C_0 16.35 Hz */
#define NOTE_CS0   (1)    /**< CS_0 sharp 17.32 Hz */
#define NOTE_D0    (2)    /**< D_0 18.35 Hz */
#define NOTE_DS0   (3)    /**< DS_0 sharp 19.45 Hz */
#define NOTE_E0    (4)    /**< E_0 20.60 Hz */
#define NOTE_F0    (5)    /**< F_0 21.83 Hz */
#define NOTE_FS0   (6)    /**< FS_0 sharp 23.12 Hz */
#define NOTE_G0    (7)    /**< G_0 24.50 Hz */
#define NOTE_GS0   (8)    /**< GS_0 sharp 25.96 Hz */
#define NOTE_A0    (9)    /**< A_0 27.50 Hz */
#define NOTE_AS0   (10)   /**< AS_0 sharp 29.14 Hz */
#define NOTE_B0    (11)   /**< B_0 30.87 Hz */

#define NOTE_C1    (12)   /**< C_1 32.70 Hz */
#define NOTE_CS1   (13)   /**< CS_1 sharp 34.65 Hz */
#define NOTE_D1    (14)   /**< D_1 36.71 Hz */
#define NOTE_DS1   (15)   /**< DS_1 sharp 38.89 Hz */
#define NOTE_E1    (16)   /**< E_1 41.20 Hz */
#define NOTE_F1    (17)   /**< F_1 43.65 Hz */
#define NOTE_FS1   (18)   /**< FS_1 sharp 46.25 Hz */
#define NOTE_G1    (19)   /**< G_1 49.00 Hz */
#define NOTE_GS1   (20)   /**< GS_1 sharp 51.91 Hz */
#define NOTE_A1    (21)   /**< A_1 55.00 Hz */
#define NOTE_AS1   (22)   /**< AS_1 sharp 58.27 Hz */
#define NOTE_B1    (23)   /**< B_1 61.74 Hz */

#define NOTE_C2    (24)   /**< C_2 65.41 Hz */
#define NOTE_CS2   (25)   /**< CS_2 sharp 69.30 Hz */
#define NOTE_D2    (26)   /**< D_2 73.42 Hz */
#define NOTE_DS2   (27)   /**< DS_2 sharp 77.78 Hz */
#define NOTE_E2    (28)   /**< E_2 82.41 Hz */
#define NOTE_F2    (29)   /**< F_2 87.31 Hz */
#define NOTE_FS2   (30)   /**< FS_2 sharp 92.50 Hz */
#define NOTE_G2    (31)   /**< G_2 98.00 Hz */
#define NOTE_GS2   (32)   /**< GS_2 sharp 103.83 Hz */
#define NOTE_A2    (33)   /**< A_2 110.00 Hz */
#define NOTE_AS2   (34)   /**< AS_2 sharp 116.54 Hz */
#define NOTE_B2    (35)   /**< B_2 123.47 Hz */

#define NOTE_C3    (36)   /**< C_3 130.81 Hz */
#define NOTE_CS3   (37)   /**< CS_3 sharp 138.59 Hz */
#define NOTE_D3    (38)   /**< D_3 146.83 Hz */
#define NOTE_DS3   (39)   /**< DS_3 sharp 155.56 Hz */
#define NOTE_E3    (40)   /**< E_3 164.81 Hz */
#define NOTE_F3    (41)   /**< F_3 174.61 Hz */
#define NOTE_FS3   (42)   /**< FS_3 sharp 185.00 Hz */
#define NOTE_G3    (43)   /**< G_3 196.00 Hz */
#define NOTE_GS3   (44)   /**< GS_3 sharp 207.65 Hz */
#define NOTE_A3    (45)   /**< A_3 220.00 Hz */
#define NOTE_AS3   (46)   /**< AS_3 sharp 233.08 Hz */
#define NOTE_B3    (47)   /**< B_3 246.94 Hz */

#define NOTE_C4    (48)   /**< C_4 261.63 Hz */
#define NOTE_CS4   (49)   /**< CS_4 sharp 277.18 Hz */
#define NOTE_D4    (50)   /**< D_4 293.66 Hz */
#define NOTE_DS4   (51)   /**< DS_4 sharp 311.13 Hz */
#define NOTE_E4    (52)   /**< E_4 329.63 Hz */
#define NOTE_F4    (53)   /**< F_4 349.23 Hz */
#define NOTE_FS4   (54)   /**< FS_4 sharp 369.99 Hz */
#define NOTE_G4    (55)   /**< G_4 392.00 Hz */
#define NOTE_GS4   (56)   /**< GS_4 sharp 415.30 Hz */
#define NOTE_A4    (57)   /**< A_4 440.00 Hz */
#define NOTE_AS4   (58)   /**< AS_4 sharp 466.16 Hz */
#define NOTE_B4    (59)   /**< B_4 493.88 Hz */

#define NOTE_C5    (60)   /**< C_5 523.25 Hz */
#define NOTE_CS5   (61)   /**< CS_5 sharp 554.37 Hz */
#define NOTE_D5    (62)   /**< D_5 587.33 Hz */
#define NOTE_DS5   (63)   /**< DS_5 sharp 622.25 Hz */
#define NOTE_E5    (64)   /**< E_5 659.26 Hz */
#define NOTE_F5    (65)   /**< F_5 698.46 Hz */
#define NOTE_FS5   (66)   /**< FS_5 sharp 739.99 Hz */
#define NOTE_G5    (67)   /**< G_5 783.99 Hz */
#define NOTE_GS5   (68)   /**< GS_5 sharp 830.61 Hz */
#define NOTE_A5    (69)   /**< A_5 880.00 Hz */
#define NOTE_AS5   (70)   /**< AS_5 sharp 932.33 Hz */
#define NOTE_B5    (71)   /**< B_5 987.77 Hz */

#define NOTE_C6    (72)   /**< C_6 1046.50 Hz */
#define NOTE_CS6   (73)   /**< CS_6 sharp 1108.73 Hz */
#define NOTE_D6    (74)   /**< D_6 1174.66 Hz */
#define NOTE_DS6   (75)   /**< DS_6 sharp 1244.51 Hz */
#define NOTE_E6    (76)   /**< E_6 1318.51 Hz */
#define NOTE_F6    (77)   /**< F_6 1396.91 Hz */
#define NOTE_FS6   (78)   /**< FS_6 sharp 1479.98 Hz */
#define NOTE_G6    (79)   /**< G_6 1567.98 Hz */
#define NOTE_GS6   (80)   /**< GS_6 sharp 1661.22 Hz */
#define NOTE_A6    (81)   /**< A_6 1760.00 Hz */
#define NOTE_AS6   (82)   /**< AS_6 sharp 1864.66 Hz */
#define NOTE_B6    (83)   /**< B_6 1975.53 Hz */

#define NOTE_C7    (84)   /**< C_7 2093.00 Hz */
#define NOTE_CS7   (85)   /**< CS_7 sharp 2217.46 Hz */
#define NOTE_D7    (86)   /**< D_7 2349.32 Hz */
#define NOTE_DS7   (87)   /**< DS_7 sharp 2489.02 Hz */
#define NOTE_E7    (88)   /**< E_7 2637.02 Hz */
#define NOTE_F7    (89)   /**< F_7 2793.83 Hz */
#define NOTE_FS7   (90)   /**< FS_7 sharp 2959.96 Hz */
#define NOTE_G7    (91)   /**< G_7 3135.96 Hz */
#define NOTE_GS7   (92)   /**< GS_7 sharp 3322.44 Hz */
#define NOTE_A7    (93)   /**< A_7 3520.00 Hz */
#define NOTE_AS7   (94)   /**< AS_7 sharp 3729.31 Hz */
#define NOTE_B7    (95)   /**< B_7 3951.07 Hz */

#define NOTE_C8    (96)   /**< C_8 4186.01 Hz */
#define NOTE_CS8   (97)   /**< CS_8 sharp 4434.92 Hz */
#define NOTE_D8    (98)   /**< D_8 4698.64 Hz */
#define NOTE_DS8   (99)   /**< DS_8 sharp 4978.03 Hz */
#define NOTE_E8    (100)  /**< E_8 5274.04 Hz */
#define NOTE_F8    (101)  /**< F_8 5587.65 Hz */
#define NOTE_FS8   (102)  /**< FS_8 sharp 5919.91 Hz */
#define NOTE_G8    (103)  /**< G_8 6271.93 Hz */
#define NOTE_GS8   (104)  /**< GS_8 sharp 6644.88 Hz */
#define NOTE_A8    (105)  /**< A_8 7040.00 Hz */
#define NOTE_AS8   (106)  /**< AS_8 sharp 7458.62 Hz */
#define NOTE_B8    (107)  /**< B_8 7902.13 Hz */

#ifdef __cplusplus
};
#endif
