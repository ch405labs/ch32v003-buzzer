/**
 * @author  Miaou (based on Florian Schütz based on E. Brombaugh original)
 * @brief   Melody library for BCD_0o27
 *
 * @copyright Copyright (c) 2024 Miaou, released under MIT license
 *
 * Based on buzzer library from 2023 Florian Schütz, released under MIT license
 * Based on original from 2022 by Stefan Wagner: https://github.com/wagiminator
 *
 * See ch32v003_melody.h
 */

#include <math.h>

#include "ch32v003_melody.h"

// Buzzer

void melody_init() {
    GPIO_port_enable(PORT_BUZZ);
    GPIO_pinMode(GPIOv_from_PORT_PIN(PORT_BUZZ, PIN_BUZZ), GPIO_pinMode_O_pushPull, GPIO_Speed_10MHz);
    GPIO_digitalWrite(GPIOv_from_PORT_PIN(PORT_BUZZ, PIN_BUZZ), high);

    // Setup the interrupt
	NVIC_EnableIRQ(SysTicK_IRQn);
}

/** Internal structure for playing a note */
typedef struct {
    uint32_t halfPeriod; /**< Time between interruptions in ticks, lower notes require more than 64k ticks */
    uint32_t counter; /**< Duration of the note (remaining number of times the note requires an interruption) */
} NoteState;

/** Internal structure describing the state of the melody player */
typedef struct {
    NoteState notes[2];
    uint8_t iBuffer;
    uint8_t failure; /**< Indicate that the next interrupt could not be planned because we took too much time to execute the current one (would spin lock) */
    uint16_t iMelody;
    Note *melody;
    uint8_t bpm;
} MelodyState;

MelodyState g_melodyState = {};

static inline void next_interrupt(uint32_t ticks)
{
	SysTick->SR = 0;
	SysTick->CMP += ticks;
    /* Detect out-of-budget interrupt */
    if( ((int32_t)( SysTick->CMP - SysTick->CNT )) > 0)
	    SysTick->CTLR |= (1<<1);
    else
        g_melodyState.failure = 1;
}

extern inline void melody_irq(void)
{
    /* Beware that all paths do SystTick->SR = 0 */
    NoteState *note = &g_melodyState.notes[g_melodyState.iBuffer];

    /* Switch the level of the signal, either low (0) or high (1).
     * We should finish with high (no current in the buzzer) so we use the inverse of the (counter%2).
     * As this is a macro, we have to compute the value beforehand... */
    if(note->counter%2)
#ifdef SOUND_MUTE
        GPIO_digitalWrite(GPIOv_from_PORT_PIN(PORT_BUZZ, PIN_BUZZ), high);
#else
        GPIO_digitalWrite(GPIOv_from_PORT_PIN(PORT_BUZZ, PIN_BUZZ), low);
#endif
    else
        GPIO_digitalWrite(GPIOv_from_PORT_PIN(PORT_BUZZ, PIN_BUZZ), high);

    if(note->counter) {
        /* If there remains half periods to play, plan the next interrupt */
        if(note->counter == -2)
            /* Implements silence */
            note->counter = 0;
        else
            /* Nominal case */
            --note->counter;
        next_interrupt(note->halfPeriod);
    } else {
        /* Otherwise plan the next note, if any */
        NoteState *next = &g_melodyState.notes[1-g_melodyState.iBuffer];
        if(next->counter) {
            g_melodyState.iBuffer = 1-g_melodyState.iBuffer;
            next_interrupt(next->halfPeriod);
        } else {
            SysTick->SR = 0;
        }
    }
}

/**
 * @brief Queue the note in the next note buffer
 * @param now Replace the current note (may glitch the sound)
 */
void queue_note(NoteState *note, uint8_t now)
{
    /* If not now but the current note is finished, use the current note, so now becomes true! */
    NoteState *current = &g_melodyState.notes[g_melodyState.iBuffer];
    NoteState *next = &g_melodyState.notes[1-g_melodyState.iBuffer];
    if(current->counter == 0)
        now = 1;
    
    /* If the melody was finished, it will stay stopped so we should plan the next IRQ before overwritting it */
    if(current->counter == 0) {
        SysTick->CMP = SysTick->CNT;
        next_interrupt(note->halfPeriod);
    }

    NoteState *target = now ? current : next;
    *target = *note;
}

/* These notes are used for precise computation of the period, the correspond to C8 to B8
 * They are uint16_t to limit the computation to 1 uint32_t (TICKS_PER_SEC) and 12 uint16_t */
const uint16_t FREQ_x8[] = { 4186, 4435, 4699, 4978, 5274, 5588, 5920, 6272, 6645, 7040, 7459, 7902 };
static inline void note2NoteState(Note *note, NoteState *tgt)
{
    if(note->pitch) {
        //uint32_t period = roundf(TICKS_PER_SEC/powf(REF_C0, note->pitch/12.f));
        uint32_t period = ((uint32_t) TICKS_PER_SEC)/FREQ_x8[note->pitch%12]; /* This gives the period if it was octave 8 */
        period <<= (8-note->pitch/12);
        tgt->halfPeriod = period/2;
        // TICKS_PER_SEC is too high, and you can only mutiply it by 715 before wrapping, so duration * 60 overshoot that
        // So we reduce it, as we know it is at least divisible by 1000, so we can divide by 10 and it should be enough for 80*60=4800
        tgt->counter = 10 * ((uint32_t)(note->durationEigths * (TICKS_PER_SEC/10) * 60) / (uint32_t)(DIVIDER_DURATION * tgt->halfPeriod * g_melodyState.bpm));
        if(tgt->counter < 2) {
            /* We have a problem: the period of the note is longer than its duration...
             * Here we decide to play it at least once */
            tgt->counter = 2;
        }
        /* The last period is played (when counter==0) so we remove 1 to the count to compensate for it */
        --tgt->counter;
    } else {
        /* Without pitch, its a silence */
        /* I have some doubt about the 1/2 here, but it works better... */
        tgt->halfPeriod = 10 * ((uint32_t)(note->durationEigths * (TICKS_PER_SEC/10) * 60) / (uint32_t)(DIVIDER_DURATION * 2 * g_melodyState.bpm));
        tgt->counter = -2;  /* (minus 2 so that it is even, and the GPIO stays high)*/
    }
}

void melody_start(Note *notes, uint8_t bpm)
{
    g_melodyState.iMelody = 0;
    g_melodyState.melody = notes;
    g_melodyState.failure = 0;
    g_melodyState.bpm = bpm;

    /* queue_note handles choosing the right buffer and planning the next interrupt if needed... */
    NoteState state;
    note2NoteState(&notes[0], &state);
    queue_note(&state, 0);
}

void melody_stop()
{
    /* Calling stop then start seems equivalent to making queue_note(now) public */
    g_melodyState.iMelody = 0;
    g_melodyState.melody = 0;
    g_melodyState.notes[0].counter = 0;
    g_melodyState.notes[1].counter = 0;
}

uint8_t melody_step(void)
{
    uint8_t status = 0;

    if(g_melodyState.failure)
        return MELODY_FAILED|MELODY_FINISHED;

    /* This is not thread safe so the iBuffer or counter may change while we look at current.counter
     * So we can't assume that counter == 0 is still the current counter, so we have to check both */
    if(g_melodyState.notes[0].counter == 0 && g_melodyState.notes[1].counter == 0)
        status |= MELODY_FINISHED;

    /* Avoids NULL ptr dereference */
    if(g_melodyState.melody == 0)
        return status | MELODY_END;

    Note *current = &g_melodyState.melody[g_melodyState.iMelody];
    if(current->pitch == 0 && current->durationEigths == 0)
        return status | MELODY_END;

    Note *next = &g_melodyState.melody[g_melodyState.iMelody+1];
    uint8_t nextIsEnd = next->pitch == 0 && next->durationEigths == 0;

    /* queue a note if needed, i.e. if the other NoteState is done */
    if(g_melodyState.notes[1-g_melodyState.iBuffer].counter == 0) {
        if(nextIsEnd)
            status |= MELODY_END;
        else {
            NoteState state;
            note2NoteState(next, &state);
            queue_note(&state, 0);
        }
        ++g_melodyState.iMelody;
        status |= MELODY_NEXT;
    }

    return status;
}
